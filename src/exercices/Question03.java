package exercices;


import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import java.util.Map;
import java.util.TreeMap;
import static utilitaires.UtilDojo.categories;
import static utilitaires.UtilDojo.determineCategorie;

public class Question03 {

  public static void main(String[] args) {
      
      Map<String,Integer> repartCagtegPoids= new TreeMap(); 
      
      System.out.println("\nCatégories de poids comportant 3 personnes ou plus \n");
       
        Integer val;
         for(Personne pers : listeDesPersonnes){  
            String categorie;

            categorie=determineCategorie( pers.sexe, pers.poids ); 
           
           if(repartCagtegPoids.containsKey(categorie))
           {
               repartCagtegPoids.put(categorie,repartCagtegPoids.get(categorie)+1);
           }else{
               repartCagtegPoids.put(categorie,1);
           }
           
         }
          for(Map.Entry e: repartCagtegPoids.entrySet()){
          val = (Integer) e.getValue();
              if(val>=3)
              {
              System.out.printf("%-20s : %3d\n",e.getKey(),e.getValue() );
      
     
              }  
        }
      
   }
}



