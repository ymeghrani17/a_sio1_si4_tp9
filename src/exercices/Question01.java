package exercices;


import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;


public class Question01 {

  public static void main(String[] args) {
      
      Map<String,Integer> repartVille= new TreeMap(); 
     
      System.out.println("\nNombre de personnes par villes\n");
   
      String ville;
          for(Personne pers : listeDesPersonnes){    
           ville=pers.ville;
           
           if(repartVille.containsKey(ville))
           {
               repartVille.put(ville,repartVille.get(ville)+1);
           }else{
               repartVille.put(ville,1);
           }
           
         }
          for(Entry e: repartVille.entrySet()){
              System.out.printf("%-20s : %3d\n",e.getKey(),e.getValue() );
          }
          
    }
}