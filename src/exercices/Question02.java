package exercices;


import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import java.util.Map;
import java.util.TreeMap;
import  static utilitaires.UtilDojo.categories;
import static utilitaires.UtilDojo.determineCategorie;

public class Question02 {

  public static void main(String[] args) {
      
      Map<String,Integer> repartCagtegPoids= new TreeMap(); 
     
      
      // pour vous aider !
      
      // on accède à toutes les catégories de poids comme cela:
      
      
      for (String cat :categories ){
      
          System.out.println(cat);
          repartCagtegPoids.put(cat,0);
      }
      
      
      
      System.out.println("\nNombre de personnes par catégories de poids\n");

       String categorie;
          for(Personne pers : listeDesPersonnes){
           categorie=determineCategorie( pers.sexe, pers.poids ); 
           repartCagtegPoids.put(categorie,repartCagtegPoids.get(categorie)+1);

          }  
      for(Map.Entry e: repartCagtegPoids.entrySet()){
              System.out.printf("%-20s : %3d\n",e.getKey(),e.getValue() );
          }
  }
}
