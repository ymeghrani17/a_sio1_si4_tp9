package utilitaires;

public class UtilDojo {
      
   public  static  String[] categories    = { "super-légers","mi-légers","légers",
                                             "mi-moyens","moyens",
                                             "mi-lourd","lourds"
                                            };
  
   public static String determineCategorie(String sexe, int poids){
     
       return categories[determineIndiceCategorie(sexe,poids)];       
   }  
   
   // pour la question 10
   public static int    determineIndiceCategorie(String sexe, int poids){
     

     int i;
         
     if (  sexe.equals("M") )
     {         
        i = chercheIndice( poids, limitesHommes );   
     }   
     else
     {   
        i = chercheIndice( poids, limitesFemmes );
     }   
         
     return i; 
    
   }  
   
   //<editor-fold defaultstate="collapsed" desc="RESTE DU CODE">  
   private static int    chercheIndice(int poids, int[] tableauLimites ){
   
      int i;
      
      for(i=0;i<tableauLimites.length;i++)
      {         
         if( poids<tableauLimites[i] )
         {  
            break;
         }
      }
   
      return i;
   }  
   
   static  int[]    limitesHommes = {60, 66, 73, 81, 90, 100};
   static  int[]    limitesFemmes = {48, 52, 57, 63, 70, 78};
   
   //</editor-fold>

}
   



