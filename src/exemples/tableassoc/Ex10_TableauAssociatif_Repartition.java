
package exemples.tableassoc;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Ex10_TableauAssociatif_Repartition {

    public static void main(String[] args) {
       
        // Déclaration et initialisation 
        // d'un tableau d'entiers à une dimension.
        
        Integer[ ] notes={
                           12, 4, 8,15,12,10,2, 8,14,13,12,12,14,
                           15,16, 2,11,10,13,16,12,11,19,13, 7,11,
                           20,12,13,14,15, 9, 3, 9,10, 3,16,13,19,
                           13, 8,15, 7, 8,11,18,10, 9,14,15,12,10,
                           15, 8, 6, 4,10,0,16, 8, 4,18,13, 7,12
                         };
        
        // On déclare le tableau associatif repart
        Map<Integer, Integer> repart= new TreeMap();
                
        // On remplit le tableau associatif repart
        // avec 21 entrées pour les clés de 0 à 20
        // c'est a dire que chaque clé représente une note susceptible de se trouver dans le tableau notes
        // a chaque clé on associe pour l'instant 0
        for (Integer n=0; n<=20; n++){ repart.put(n, 0);}
        
        
        // On parcourt le tableau note 
        for( Integer n : notes){
        
           // on acccède à la valeur associée à la clé n du tableau repart et on lui ajoute 1  
           Integer val=repart.get(n)+1;
           // on range val dans l'entrée associée à la clé n
           repart.put(n, val); 
        }
        
        System.out.println("Répartition des notes contenues dans le tableau notes\n");
        
        
        // On affiche la liste des entrées
        // qui donne pour chaque note possible, le nombre de fois qu'on l'a trouvée  dans le tableau notes 
        for( Entry  e: repart.entrySet()){
        
            System.out.printf("Nombre de %3d: %3d\n",e.getKey(),e.getValue() );
        }
        System.out.println();
       
    }
}
